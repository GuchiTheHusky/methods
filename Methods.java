import java.util.Arrays;
import java.util.concurrent.ThreadLocalRandom;

public class Methods {

    // 1.
    public void printArray(char[] array) {
        System.out.println(Arrays.toString(array));
    }

    // 2.
    public char[] intToCharArray(int[] numbers) {
        char[] charArray = new char[numbers.length];
        for (int i = 0; i < numbers.length; i++) {
            charArray[i] = (char) numbers[i];
        }
        return charArray;
    }

    // 3.
    public int maxValue(int a, int b) {
        if (a > b) return a;
        return b;
    }

    // 4.
    public int maxValue(int a, int b, int c) {
        if (a > b && a > c) return a;
        else if (b > c) return b;
        return c;
    }

    // 5.
    public int maxValue(int a, int b, int c, int d, int e) {
        if (a > b && a > c && a > d && a > e) return a;
        else if (b > c && b > d && b > e) return b;
        else if (c > d && c > e) return c;
        else if (d > e) return d;
        else return e;
    }

    // 6.
    public String charToString(char[] array) {
        StringBuilder str = new StringBuilder();
        for (char c : array) {
            str.append(c);
        }
        return str.toString();
    }

    // 7.
    public int indexOf(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) return i;
        }
        return -1;
    }

    // 8.
    public int lastIndexOf(int[] array, int value) {
        for (int i = array.length - 1; i >= 0; i--) {
            if (array[i] == value) return i;
        }
        return -1;
    }

    // 9.
    public int getFactorial(int value) {
        int factorial = 1;
        for (int i = 1; i <= value; i++) {
            factorial *= i;
        }
        return factorial;
    }

    // 10.
    public boolean isLeapYear(int year) {
        return year % 4 == 0;
    }

    // 11.
    public void multiplesNumbers(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] % value == 0) System.out.print(array[i] + " ");
            ;
        }
    }

    // 12.
    public int[] bubbleSort(int[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] < array[j]) {
                    int temp = array[i];
                    array[i] = array[j];
                    array[j] = temp;
                }
            }
        }
        return array;
    }

    // 13.
    public boolean isElementsRepeat(byte[] array) {
        for (int i = 0; i < array.length; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] == array[j]) return true;
            }
        }
        return false;
    }

    // 14.
    public int[] multiplyArray(int[] first, int[] second) {
        if (first.length != second.length) return new int[]{-1};
        else {
            int[] result = new int[first.length];
            for (int i = 0; i < result.length; i++) {
                result[i] = first[i] * second[i];
            }
            return result;
        }
    }
    // 15.
    public int[] getUniqueNumbers(int[] first, int[] second) {
        int size = 0;
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) size++;
        }
        for (int i = 0; i < second.length; i++) {
            if (second[i] != first[i]) size++;
        }

        int[] result = new int[size];
        int start = 0;
        for (int i = 0; i < first.length; i++) {
            if (first[i] != second[i]) {
                result[start] = first[i];
                start++;
            }
        }
        int end = result.length - 1;
        for (int i = 0; i < second.length; i++) {
            if (second[i] != first[i]) {
                result[end] = second[i];
                end--;
            }
        }
        return result;
    }

    // 16.
    public int[] reverseArray(int[] array) {
        for (int i = 0; i < array.length / 2; i++) {
            int temp = array[i];
            array[i] = array[array.length - 1 - i];
            array[array.length - 1 - i] = temp;
        }
        return array;
    }

    // 17.
    public int[] randomArray(int size, int origin, int bound) {
        if (size <= 0 || origin >= bound) return new int[]{-1};
        else {
            int[] result = new int[size];
            for (int i = 0; i < result.length; i++) {
                result[i] = ThreadLocalRandom.current().nextInt(origin, bound);
            }
            return result;
        }
    }

    // 18.
    public boolean isEqualsArrays(char[] first, char[] second) {
        StringBuilder strFirst = new StringBuilder();
        StringBuilder strSecond = new StringBuilder();

        for (char c : first) {
            strFirst.append(c);
        }
        for (char c : second) {
            strSecond.append(c);
        }
        return strFirst.toString().equals(strSecond.toString());
    }


}


//        ++ 1) принимает массив чаров, выводит его на экран
//        ++ 2) принимает массив интов, возвращает массив чаров, каждый символ в позиции массива соответствует
//        коду символа передаваемого инта
//        ++ 3) приминает 2 инта, а и б, возвращает большее их этих 2х чисел
//        ++ 4) принимает 3 инта, возвращает большее из них
//        ++ 5) приминает 5 интов, возвращает большее из них
//        ++ 6) принимает массив чаров, возвращает строку состоящую из символов массива
//        ++ 7) принимает массив интов, и значение типа инт, возвращает индек
//        массива в котором значение совпадает с передаваемым, начиная с начала массива.
//        Если значения в массиве нет возвращает -1
//        ++ 8) принимает массив интов, и значение типа инт, возвращает индекс
//        массива в котором значение совпадает с передаваемым, начиная с конца массива.
//        Если значения в массиве нет возвращает -1
//        ++ 9) метод принимает инт, и возвращает факториал от заданого инта
//        ++ 10) принимает инт год, и возвращает тру если год высокосный
//        ++ 11) приминает массив интов и число, выводит на екран только елементы массива которые кратны этому числу
//        ++ 12) метод принимает массив интов сортирует его по возрастанию
//        ++ 13) принимает массив байт, если в массиве есть повторяющиеся елементы, возвращает тру
//        ++ 14) принимает два массива интов одинаковых по длинне, возращает массив интов
//        который состоит из перемноженных елементов входящих массивов
//        ++ 15) принимает два массива интов, возвращает массив из елементов которые не совпадают в массивах
//        ++ 16) принимает масив интов, возвращает его же но в реверсном порядке
//        ++ 17) принимает 3 инта:
//        - размер выходного массива
//        - нижняя граница
//        - верхняя граница
//        возвращает массив интов заданой длинный, который содержит случайные числа от нижней границы до верхней границы"
//        ++ 18) принимает 2 массива чаров, проверяет есть ли в 1 массиве,
//        такая же последовательность символов которую представляет собой второй массив. Возвращает булеан
